desc 'Generate slugs for existing models'

require 'active_record'

namespace :mail_suite do
  task :generate_slugs => :environment do

    Admin.find_each(&:save)
    Alias.find_each(&:save)
    Domain.find_each(&:save)
    DomainAdmin.find_each(&:save)
    Log.find_each(&:save)
    Mailbox.find_each(&:save)
    Vacation.find_each(&:save)

  end
end
