class AddSlugToAliases < ActiveRecord::Migration
  def change
    add_column :alias, :slug, :string
    add_index :alias, :slug
  end
end
