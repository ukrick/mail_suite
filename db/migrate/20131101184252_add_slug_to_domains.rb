class AddSlugToDomains < ActiveRecord::Migration
  def change
    add_column :domain, :slug, :string
    add_index :domain, :slug
  end
end
