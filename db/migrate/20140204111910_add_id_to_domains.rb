class AddIdToDomains < ActiveRecord::Migration
  execute 'ALTER TABLE `domain` DROP PRIMARY KEY'

  def change
    change_column :domain, :domain, :string, default: '', null: false
    add_column :domain, :id, :primary_key
    add_index :domain, :id
  end
end
