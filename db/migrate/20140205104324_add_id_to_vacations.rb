class AddIdToVacations < ActiveRecord::Migration
  execute 'ALTER TABLE `vacation` DROP PRIMARY KEY'

  def change
    change_column :vacation, :email, :string, default: '', null: false
    add_column :vacation, :id, :primary_key
    add_index :vacation, :id
  end
end
