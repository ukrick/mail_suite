class AddIdToAliases < ActiveRecord::Migration
  execute 'ALTER TABLE `alias` DROP PRIMARY KEY'

  def change
    change_column :alias, :address, :string, default: '', null: false
    add_column :alias, :id, :primary_key
    add_index :alias, :id
  end
end
