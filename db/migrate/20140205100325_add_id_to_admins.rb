class AddIdToAdmins < ActiveRecord::Migration
  execute 'ALTER TABLE `admin` DROP PRIMARY KEY'

  def change
    change_column :admin, :username, :string, default: '', null: false
    add_column :admin, :id, :primary_key
    add_index :admin, :id
  end
end
