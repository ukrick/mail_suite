class AddIdToLogs < ActiveRecord::Migration
  def change
    add_column :log, :id, :primary_key
    add_index :log, :id
  end
end
