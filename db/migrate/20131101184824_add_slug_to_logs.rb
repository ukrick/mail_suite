class AddSlugToLogs < ActiveRecord::Migration
  def change
    add_column :log, :slug, :string
    add_index :log, :slug
  end
end
