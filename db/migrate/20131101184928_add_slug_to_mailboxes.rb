class AddSlugToMailboxes < ActiveRecord::Migration
  def change
    add_column :mailbox, :slug, :string
    add_index :mailbox, :slug
  end
end
