class AddSlugToVacations < ActiveRecord::Migration
  def change
    add_column :vacation, :slug, :string
    add_index :vacation, :slug
  end
end
