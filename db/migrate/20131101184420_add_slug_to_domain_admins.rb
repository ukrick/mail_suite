class AddSlugToDomainAdmins < ActiveRecord::Migration
  def change
    add_column :domain_admins, :slug, :string
    add_index :domain_admins, :slug
  end
end
