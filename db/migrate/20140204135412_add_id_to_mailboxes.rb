class AddIdToMailboxes < ActiveRecord::Migration
  execute 'ALTER TABLE `mailbox` DROP PRIMARY KEY'

  def change
    change_column :mailbox, :username, :string, default: '', null: false
    add_column :mailbox, :id, :primary_key
    add_index :mailbox, :id
  end
end
