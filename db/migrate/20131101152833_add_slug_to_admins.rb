class AddSlugToAdmins < ActiveRecord::Migration
  def change
    add_column :admin, :slug, :string
    add_index :admin, :slug
  end
end
