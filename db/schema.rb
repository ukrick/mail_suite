# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140205104324) do

  create_table "admin", force: :cascade do |t|
    t.string   "username", limit: 255, default: "",   null: false
    t.string   "password", limit: 255, default: "",   null: false
    t.datetime "created",                             null: false
    t.datetime "modified",                            null: false
    t.boolean  "active",               default: true, null: false
    t.string   "slug",     limit: 255
  end

  add_index "admin", ["id"], name: "index_admin_on_id", using: :btree
  add_index "admin", ["slug"], name: "index_admin_on_slug", using: :btree
  add_index "admin", ["username"], name: "username", using: :btree

  create_table "alias", force: :cascade do |t|
    t.string   "address",  limit: 255,   default: "",   null: false
    t.text     "goto",     limit: 65535,                null: false
    t.string   "domain",   limit: 255,   default: "",   null: false
    t.datetime "created",                               null: false
    t.datetime "modified",                              null: false
    t.boolean  "active",                 default: true, null: false
    t.string   "slug",     limit: 255
  end

  add_index "alias", ["address"], name: "address", using: :btree
  add_index "alias", ["id"], name: "index_alias_on_id", using: :btree
  add_index "alias", ["slug"], name: "index_alias_on_slug", using: :btree

  create_table "domain", force: :cascade do |t|
    t.string   "domain",      limit: 255, default: "",    null: false
    t.string   "description", limit: 255, default: "",    null: false
    t.integer  "aliases",     limit: 4,   default: 0,     null: false
    t.integer  "mailboxes",   limit: 4,   default: 0,     null: false
    t.integer  "maxquota",    limit: 4,   default: 0,     null: false
    t.string   "transport",   limit: 255
    t.boolean  "backupmx",                default: false, null: false
    t.datetime "created",                                 null: false
    t.datetime "modified",                                null: false
    t.boolean  "active",                  default: true,  null: false
    t.string   "slug",        limit: 255
  end

  add_index "domain", ["domain"], name: "domain", using: :btree
  add_index "domain", ["id"], name: "index_domain_on_id", using: :btree
  add_index "domain", ["slug"], name: "index_domain_on_slug", using: :btree

  create_table "domain_admins", id: false, force: :cascade do |t|
    t.string   "username", limit: 255, default: "",   null: false
    t.string   "domain",   limit: 255, default: "",   null: false
    t.datetime "created",                             null: false
    t.boolean  "active",               default: true, null: false
    t.string   "slug",     limit: 255
  end

  add_index "domain_admins", ["slug"], name: "index_domain_admins_on_slug", using: :btree
  add_index "domain_admins", ["username"], name: "username", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "log", force: :cascade do |t|
    t.datetime "timestamp",                          null: false
    t.string   "username",  limit: 255, default: "", null: false
    t.string   "domain",    limit: 255, default: "", null: false
    t.string   "action",    limit: 255, default: "", null: false
    t.string   "data",      limit: 255, default: "", null: false
    t.string   "slug",      limit: 255
  end

  add_index "log", ["id"], name: "index_log_on_id", using: :btree
  add_index "log", ["slug"], name: "index_log_on_slug", using: :btree
  add_index "log", ["timestamp"], name: "timestamp", using: :btree

  create_table "mailbox", force: :cascade do |t|
    t.string   "username", limit: 255, default: "",   null: false
    t.string   "password", limit: 255, default: "",   null: false
    t.string   "name",     limit: 255, default: "",   null: false
    t.string   "maildir",  limit: 255, default: "",   null: false
    t.integer  "quota",    limit: 4,   default: 0,    null: false
    t.string   "domain",   limit: 255, default: "",   null: false
    t.datetime "created",                             null: false
    t.datetime "modified",                            null: false
    t.boolean  "active",               default: true, null: false
    t.string   "slug",     limit: 255
  end

  add_index "mailbox", ["id"], name: "index_mailbox_on_id", using: :btree
  add_index "mailbox", ["slug"], name: "index_mailbox_on_slug", using: :btree
  add_index "mailbox", ["username"], name: "username", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "vacation", force: :cascade do |t|
    t.string   "email",   limit: 255,   default: "",   null: false
    t.string   "subject", limit: 255,   default: "",   null: false
    t.text     "body",    limit: 65535,                null: false
    t.text     "cache",   limit: 65535,                null: false
    t.string   "domain",  limit: 255,   default: "",   null: false
    t.datetime "created",                              null: false
    t.boolean  "active",                default: true, null: false
    t.string   "slug",    limit: 255
  end

  add_index "vacation", ["email"], name: "email", using: :btree
  add_index "vacation", ["id"], name: "index_vacation_on_id", using: :btree
  add_index "vacation", ["slug"], name: "index_vacation_on_slug", using: :btree

end
