module ControllerMacros
  def login_admin
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = FactoryGirl.create(:user)
      user.remove_role :user
      user.add_role :admin
      sign_in user
    end
  end

  def login_user
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = FactoryGirl.create(:user)
      #user.confirm! # Only necessary if you are using the confirmable module
      sign_in user
    end
  end
end
