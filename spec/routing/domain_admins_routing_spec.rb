require "spec_helper"

describe DomainAdminsController do
  describe "routing" do

    it "routes to #index" do
      get("/domain_admins").should route_to("domain_admins#index")
    end

    it "routes to #new" do
      get("/domain_admins/new").should route_to("domain_admins#new")
    end

    it "routes to #show" do
      get("/domain_admins/1").should route_to("domain_admins#show", :id => "1")
    end

    it "routes to #edit" do
      get("/domain_admins/1/edit").should route_to("domain_admins#edit", :id => "1")
    end

    it "routes to #create" do
      post("/domain_admins").should route_to("domain_admins#create")
    end

    it "routes to #update" do
      put("/domain_admins/1").should route_to("domain_admins#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/domain_admins/1").should route_to("domain_admins#destroy", :id => "1")
    end

  end
end
