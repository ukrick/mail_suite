require 'spec_helper'

describe 'Admins' do

  before(:each) do
    @admin = create(:admin)
  end

  describe 'GET /admins' do
    it 'retrieves admins page for anonymous' do
      get admins_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'retrieves admins page for admin' do
      sign_in_as_a_admin
      get admins_path
      response.status.should be(200)
    end
    it 'retrieves admins page for user' do
      sign_in_as_a_user
      get admins_path
      response.status.should be(200)
    end
  end

  describe 'GET /admins/:id' do
    it 'retrieves show admin page for anonymous' do
      get "admins/#{@admin.slug}"
      response.status.should be(200)
    end
    it 'retrieves show admin page for admin' do
      sign_in_as_a_admin
      get "admins/#{@admin.slug}"
      response.status.should be(200)
    end
    it 'retrieves show admin page for user' do
      sign_in_as_a_user
      get "admins/#{@admin.slug}"
      response.status.should be(200)
    end
  end

  describe 'POST /admins' do
    it 'create admin for anonymous' do
      get new_admin_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'create admin for admin' do
      sign_in_as_a_admin
      get new_admin_path
      expect(response).to render_template(:new)

      post '/admins', :admin => attributes_for(:admin)

      expect(response).to redirect_to(assigns(:admin))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Admin was successfully created.')
      response.status.should be(200)
    end
    it 'create admin for user' do
      sign_in_as_a_user
      get new_admin_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'PATCH/PUT /admins/:id' do
    it 'update admin for anonymous' do
      get "admins/#{@admin.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'update admin for admin' do
      sign_in_as_a_admin
      get "admins/#{@admin.slug}/edit"
      expect(response).to render_template(:edit)

      put "admins/#{@admin.slug}", :admin => attributes_for(:admin)

      expect(response).to redirect_to(assigns(:admin))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Admin was successfully updated.')
      response.status.should be(200)
    end
    it 'update admin for user' do
      sign_in_as_a_user
      get "admins/#{@admin.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'DELETE /admins/:id' do
    it 'delete admin for anonymous' do
      delete "admins/#{@admin.slug}", :id => @admin.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'delete admin for admin' do
      sign_in_as_a_admin
      get admins_path

      delete "admins/#{@admin.slug}", :id => @admin.id

      expect(response).to redirect_to admins_path
      follow_redirect!
      expect(response).to render_template(:index)
      response.status.should be(200)
    end
    it 'delete admin for user' do
      sign_in_as_a_user
      get admins_path
      delete "admins/#{@admin.slug}", :id => @admin.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end
end
