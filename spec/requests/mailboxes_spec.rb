require 'spec_helper'

describe 'Mailboxes' do

  before(:each) do
    @mailbox = Mailbox.create!(username: 'example',
                               password: 'password',
                               name: 'Test Customer',
                               maildir: 'example',
                               quota: 1024000,
                               domain: 'example.com',
                               created: Time.now.utc.to_s,
                               modified: Time.now.utc.to_s)
  end

  describe 'GET /mailboxes' do
    it 'retrieves mailboxes page for anonymous' do
      get mailboxes_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'retrieves mailboxes page for mailbox' do
      sign_in_as_a_admin
      get mailboxes_path
      response.status.should be(200)
    end
    it 'retrieves mailboxes page for user' do
      sign_in_as_a_user
      get mailboxes_path
      response.status.should be(200)
    end
  end

  describe 'GET /mailboxes/:id' do
    it 'retrieves show mailbox page for anonymous' do
      get "mailboxes/#{@mailbox.slug}"
      response.status.should be(200)
    end
    it 'retrieves show mailbox page for mailbox' do
      sign_in_as_a_admin
      get "mailboxes/#{@mailbox.slug}"
      response.status.should be(200)
    end
    it 'retrieves show mailbox page for user' do
      sign_in_as_a_user
      get "mailboxes/#{@mailbox.slug}"
      response.status.should be(200)
    end
  end

  describe 'POST /mailboxes' do
    it 'create mailbox for anonymous' do
      get new_mailbox_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'create mailbox for mailbox' do
      sign_in_as_a_admin
      get new_mailbox_path
      expect(response).to render_template(:new)

      post '/mailboxes', :mailbox => attributes_for(:mailbox)

      expect(response).to redirect_to(assigns(:mailbox))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Mailbox was successfully created.')
      response.status.should be(200)
    end
    it 'create mailbox for user' do
      sign_in_as_a_user
      get new_mailbox_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'PATCH/PUT /mailboxes/:id' do
    it 'update mailbox for anonymous' do
      get "mailboxes/#{@mailbox.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'update mailbox for mailbox' do
      sign_in_as_a_admin
      get "mailboxes/#{@mailbox.slug}/edit"
      expect(response).to render_template(:edit)

      put "mailboxes/#{@mailbox.slug}", :mailbox => attributes_for(:mailbox)

      expect(response).to redirect_to(assigns(:mailbox))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Mailbox was successfully updated.')
      response.status.should be(200)
    end
    it 'update mailbox for user' do
      sign_in_as_a_user
      get "mailboxes/#{@mailbox.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'DELETE /mailboxes/:id' do
    it 'delete mailbox for anonymous' do
      delete "mailboxes/#{@mailbox.slug}", :id => @mailbox.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'delete mailbox for mailbox' do
      sign_in_as_a_admin
      get mailboxes_path

      delete "mailboxes/#{@mailbox.slug}", :id => @mailbox.id

      expect(response).to redirect_to mailboxes_path
      follow_redirect!
      expect(response).to render_template(:index)
      response.status.should be(200)
    end
    it 'delete mailbox for user' do
      sign_in_as_a_user
      get mailboxes_path
      delete "mailboxes/#{@mailbox.slug}", :id => @mailbox.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end
end
