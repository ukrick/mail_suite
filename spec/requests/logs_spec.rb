require 'spec_helper'

describe 'Domains' do

  before(:each) do
    @log = create(:log)
  end

  describe 'GET /logs' do
    it 'retrieves logs page for anonymous' do
      get logs_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'retrieves logs page for log' do
      sign_in_as_a_admin
      get logs_path
      response.status.should be(200)
    end
    it 'retrieves logs page for user' do
      sign_in_as_a_user
      get logs_path
      response.status.should be(200)
    end
  end

  describe 'GET /logs/:id' do
    it 'retrieves show log page for anonymous' do
      get "logs/#{@log.slug}"
      response.status.should be(200)
    end
    it 'retrieves show log page for log' do
      sign_in_as_a_admin
      get "logs/#{@log.slug}"
      response.status.should be(200)
    end
    it 'retrieves show log page for user' do
      sign_in_as_a_user
      get "logs/#{@log.slug}"
      response.status.should be(200)
    end
  end

  describe 'DELETE /logs/:id' do
    it 'delete log for anonymous' do
      delete "logs/#{@log.slug}", :id => @log.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'delete log for log' do
      sign_in_as_a_admin
      get logs_path

      delete "logs/#{@log.slug}", :id => @log.id

      expect(response).to redirect_to logs_path
      follow_redirect!
      expect(response).to render_template(:index)
      response.status.should be(200)
    end
    it 'delete log for user' do
      sign_in_as_a_user
      get logs_path
      delete "logs/#{@log.slug}", :id => @log.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end
end
