require 'spec_helper'

describe 'vacations' do

  before(:each) do
    @vacation = create(:vacation)
  end

  describe 'GET /vacations' do
    it 'retrieves vacations page for anonymous' do
      get vacations_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'retrieves vacations page for vacation' do
      sign_in_as_a_admin
      get vacations_path
      response.status.should be(200)
    end
    it 'retrieves vacations page for user' do
      sign_in_as_a_user
      get vacations_path
      response.status.should be(200)
    end
  end

  describe 'GET /vacations/:id' do
    it 'retrieves show vacation page for anonymous' do
      get "vacations/#{@vacation.slug}"
      response.status.should be(200)
    end
    it 'retrieves show vacation page for vacation' do
      sign_in_as_a_admin
      get "vacations/#{@vacation.slug}"
      response.status.should be(200)
    end
    it 'retrieves show vacation page for user' do
      sign_in_as_a_user
      get "vacations/#{@vacation.slug}"
      response.status.should be(200)
    end
  end

  describe 'POST /vacations' do
    it 'create vacation for anonymous' do
      get new_vacation_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'create vacation for vacation' do
      sign_in_as_a_admin
      get new_vacation_path
      expect(response).to render_template(:new)

      post '/vacations', :vacation => attributes_for(:vacation)

      expect(response).to redirect_to(assigns(:vacation))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Vacation was successfully created.')
      response.status.should be(200)
    end
    it 'create vacation for user' do
      sign_in_as_a_user
      get new_vacation_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'PATCH/PUT /vacations/:id' do
    it 'update vacation for anonymous' do
      get "vacations/#{@vacation.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'update vacation for vacation' do
      sign_in_as_a_admin
      get "vacations/#{@vacation.slug}/edit"
      expect(response).to render_template(:edit)

      put "vacations/#{@vacation.slug}", :vacation => attributes_for(:vacation)

      expect(response).to redirect_to(assigns(:vacation))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Vacation was successfully updated.')
      response.status.should be(200)
    end
    it 'update vacation for user' do
      sign_in_as_a_user
      get "vacations/#{@vacation.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'DELETE /vacations/:id' do
    it 'delete vacation for anonymous' do
      delete "vacations/#{@vacation.slug}", :id => @vacation.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'delete vacation for vacation' do
      sign_in_as_a_admin
      get vacations_path

      delete "vacations/#{@vacation.slug}", :id => @vacation.id

      expect(response).to redirect_to vacations_path
      follow_redirect!
      expect(response).to render_template(:index)
      response.status.should be(200)
    end
    it 'delete vacation for user' do
      sign_in_as_a_user
      get vacations_path
      delete "vacations/#{@vacation.slug}", :id => @vacation.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end
end
