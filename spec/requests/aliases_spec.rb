require 'spec_helper'

describe 'Aliases' do

  before(:each) do
    @alias = create(:alias)
  end

  describe 'GET /aliases' do
    it 'retrieves aliases page for anonymous' do
      get aliases_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'retrieves aliases page for admin' do
      sign_in_as_a_admin
      get aliases_path
      response.status.should be(200)
    end
    it 'retrieves aliases page for user' do
      sign_in_as_a_user
      get aliases_path
      response.status.should be(200)
    end
  end

  describe 'GET /aliases/:id' do
    it 'retrieves show alias page for anonymous' do
      get "aliases/#{@alias.slug}"
      response.status.should be(200)
    end
    it 'retrieves show alias page for admin' do
      sign_in_as_a_admin
      get "aliases/#{@alias.slug}"
      response.status.should be(200)
    end
    it 'retrieves show alias page for user' do
      sign_in_as_a_user
      get "aliases/#{@alias.slug}"
      response.status.should be(200)
    end
  end

  describe 'POST /aliases' do
    it 'create alias for anonymous' do
      get new_alias_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'create alias for admin' do
      sign_in_as_a_admin
      get new_alias_path
      expect(response).to render_template(:new)

      post '/aliases', :alias => attributes_for(:alias)

      expect(response).to redirect_to(assigns(:alias))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Alias was successfully created.')
      response.status.should be(200)
    end
    it 'create alias for user' do
      sign_in_as_a_user
      get new_alias_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'PATCH/PUT /aliases/:id' do
    it 'update alias for anonymous' do
      get "aliases/#{@alias.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'update alias for admin' do
      sign_in_as_a_admin
      get "aliases/#{@alias.slug}/edit"
      expect(response).to render_template(:edit)

      put "aliases/#{@alias.slug}", :alias => attributes_for(:alias)

      expect(response).to redirect_to(assigns(:alias))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Alias was successfully updated.')
      response.status.should be(200)
    end
    it 'update alias for user' do
      sign_in_as_a_user
      get "aliases/#{@alias.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'DELETE /aliases/:id' do
    it 'delete alias for anonymous' do
      delete "aliases/#{@alias.slug}", :id => @alias.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'delete alias for admin' do
      sign_in_as_a_admin
      get aliases_path

      delete "aliases/#{@alias.slug}", :id => @alias.id

      expect(response).to redirect_to aliases_path
      follow_redirect!
      expect(response).to render_template(:index)
      response.status.should be(200)
    end
    it 'delete alias for user' do
      sign_in_as_a_user
      get aliases_path
      delete "aliases/#{@alias.slug}", :id => @alias.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end
end
