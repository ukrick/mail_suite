require 'spec_helper'

describe 'DomainAdmins' do

  before(:each) do
    @domain_admin = create(:domain_admin)
  end

  describe 'GET /domain_admins' do
    it 'retrieves domain_admins page for anonymous' do
      get domain_admins_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'retrieves domain_admins page for domain_admin' do
      sign_in_as_a_admin
      get domain_admins_path
      response.status.should be(200)
    end
    it 'retrieves domain_admins page for user' do
      sign_in_as_a_user
      get domain_admins_path
      response.status.should be(200)
    end
  end

  describe 'GET /domain_admins/:id' do
    it 'retrieves show domain_admin page for anonymous' do
      get "domain_admins/#{@domain_admin.slug}"
      response.status.should be(200)
    end
    it 'retrieves show domain_admin page for domain_admin' do
      sign_in_as_a_admin
      get "domain_admins/#{@domain_admin.slug}"
      response.status.should be(200)
    end
    it 'retrieves show domain_admin page for user' do
      sign_in_as_a_user
      get "domain_admins/#{@domain_admin.slug}"
      response.status.should be(200)
    end
  end

  describe 'POST /domain_admins' do
    it 'create domain_admin for anonymous' do
      get new_domain_admin_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'create domain_admin for domain_admin' do
      sign_in_as_a_admin
      get new_domain_admin_path
      expect(response).to render_template(:new)

      post '/domain_admins', :domain_admin => attributes_for(:domain_admin)

      expect(response).to redirect_to(assigns(:domain_admin))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Domain admin was successfully created.')
      response.status.should be(200)
    end
    it 'create domain_admin for user' do
      sign_in_as_a_user
      get new_domain_admin_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'PATCH/PUT /domain_admins/:id' do
    it 'update domain_admin for anonymous' do
      get "domain_admins/#{@domain_admin.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'update domain_admin for domain_admin' do
      sign_in_as_a_admin
      get "domain_admins/#{@domain_admin.slug}/edit"
      expect(response).to render_template(:edit)

      put "domain_admins/#{@domain_admin.slug}", :domain_admin => attributes_for(:domain_admin)

      expect(response).to redirect_to(assigns(:domain_admin))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Domain admin was successfully updated.')
      response.status.should be(200)
    end
    it 'update domain_admin for user' do
      sign_in_as_a_user
      get "domain_admins/#{@domain_admin.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'DELETE /domain_admins/:id' do
    it 'delete domain_admin for anonymous' do
      delete "domain_admins/#{@domain_admin.slug}", :id => @domain_admin.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'delete domain_admin for domain_admin' do
      sign_in_as_a_admin
      get domain_admins_path

      delete "domain_admins/#{@domain_admin.slug}", :id => @domain_admin.id

      expect(response).to redirect_to domain_admins_path
      follow_redirect!
      expect(response).to render_template(:index)
      response.status.should be(200)
    end
    it 'delete domain_admin for user' do
      sign_in_as_a_user
      get domain_admins_path
      delete "domain_admins/#{@domain_admin.slug}", :id => @domain_admin.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end
end
