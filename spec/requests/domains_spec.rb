require 'spec_helper'

describe 'Domains' do

  before(:each) do
    @domain = create(:domain)
  end

  describe 'GET /domains' do
    it 'retrieves domains page for anonymous' do
      get domains_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'retrieves domains page for domain' do
      sign_in_as_a_admin
      get domains_path
      response.status.should be(200)
    end
    it 'retrieves domains page for user' do
      sign_in_as_a_user
      get domains_path
      response.status.should be(200)
    end
  end

  describe 'GET /domains/:id' do
    it 'retrieves show domain page for anonymous' do
      get "domains/#{@domain.slug}"
      response.status.should be(200)
    end
    it 'retrieves show domain page for domain' do
      sign_in_as_a_admin
      get "domains/#{@domain.slug}"
      response.status.should be(200)
    end
    it 'retrieves show domain page for user' do
      sign_in_as_a_user
      get "domains/#{@domain.slug}"
      response.status.should be(200)
    end
  end

  describe 'POST /domains' do
    it 'create domain for anonymous' do
      get new_domain_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'create domain for domain' do
      sign_in_as_a_admin
      get new_domain_path
      expect(response).to render_template(:new)

      post '/domains', :domain => attributes_for(:domain)

      expect(response).to redirect_to(assigns(:domain))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Domain was successfully created.')
      response.status.should be(200)
    end
    it 'create domain for user' do
      sign_in_as_a_user
      get new_domain_path
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'PATCH/PUT /domains/:id' do
    it 'update domain for anonymous' do
      get "domains/#{@domain.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'update domain for domain' do
      sign_in_as_a_admin
      get "domains/#{@domain.slug}/edit"
      expect(response).to render_template(:edit)

      put "domains/#{@domain.slug}", :domain => attributes_for(:domain)

      expect(response).to redirect_to(assigns(:domain))
      follow_redirect!

      expect(response).to render_template(:show)
      expect(response.body).to include('Domain was successfully updated.')
      response.status.should be(200)
    end
    it 'update domain for user' do
      sign_in_as_a_user
      get "domains/#{@domain.slug}/edit"
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end

  describe 'DELETE /domains/:id' do
    it 'delete domain for anonymous' do
      delete "domains/#{@domain.slug}", :id => @domain.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
    it 'delete domain for domain' do
      sign_in_as_a_admin
      get domains_path

      delete "domains/#{@domain.slug}", :id => @domain.id

      expect(response).to redirect_to domains_path
      follow_redirect!
      expect(response).to render_template(:index)
      response.status.should be(200)
    end
    it 'delete domain for user' do
      sign_in_as_a_user
      get domains_path
      delete "domains/#{@domain.slug}", :id => @domain.id
      expect(response).to redirect_to root_path
      follow_redirect!
      expect(response.body).to include('You are not authorized to access this page.')
      response.status.should be(200)
    end
  end
end
