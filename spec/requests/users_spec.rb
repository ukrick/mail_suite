require 'spec_helper'

describe 'Users' do

  describe 'GET /users' do
    it 'retrieves "Sign in" page for anonymous' do
      get '/users/sign_in'
      response.status.should be(200)
    end
    it 'retrieves users page for admin' do
      sign_in_as_a_admin
      get users_path
      response.status.should be(200)
    end
    it 'retrieves users page for user' do
      sign_in_as_a_user
      get users_path
      response.status.should be(200)
    end
  end

  describe 'GET /users/:id' do
    it 'retrieves "Sign in" page for anonymous' do
      get '/users/sign_in'
      response.status.should be(200)
    end
    it 'retrieves user page for admin' do
      sign_in_as_a_admin
      admin = User.joins(:roles).where("`roles`.`name` = 'admin'").first
      get "users/#{admin.id}"
      response.status.should be(200)
    end
    it 'retrieves user page for user' do
      sign_in_as_a_user
      user = User.joins(:roles).where("`roles`.`name` = 'user'").first
      get "users/#{user.id}"
      response.status.should be(200)
    end
  end

  describe 'GET /users/edit' do
    it 'retrieves "Sign in" page for anonymous' do
      get '/users/sign_in'
      response.status.should be(200)
    end
    it 'retrieves edit page for admin' do
      sign_in_as_a_admin
      get 'users/edit'
      response.status.should be(200)
    end
    it 'retrieves edit page for user' do
      sign_in_as_a_user
      get 'users/edit'
      response.status.should be(200)
    end
  end

  describe 'PUT /users' do
    it 'retrieves "Sign in" page for anonymous' do
      get '/users/sign_in'
      response.status.should be(200)
    end
    it 'retrieves users page for admin' do
      sign_in_as_a_admin
      get users_path
      response.status.should be(200)
    end
    it 'retrieves users page for user' do
      sign_in_as_a_user
      get users_path
      response.status.should be(200)
    end
  end

  describe 'DELETE /users/:id' do
    it 'retrieves "Sign in" page for anonymous' do
      get '/users/sign_in'
      response.status.should be(200)
    end
    it 'retrieves users page for admin' do
      sign_in_as_a_admin
      get users_path
      response.status.should be(200)
    end
    it 'retrieves users page for user' do
      sign_in_as_a_user
      get root_path
      response.status.should be(200)
    end
  end

  describe 'GET /' do
    it 'retrieves home page for anonymous' do
      get root_path
      response.status.should be(200)
    end
    it 'retrieves home page for admin' do
      sign_in_as_a_admin
      get root_path
      response.status.should be(200)
    end
    it 'retrieves home page for user' do
      sign_in_as_a_user
      get root_path
      response.status.should be(200)
    end
  end
end
