require 'spec_helper'

describe UsersController do

  login_admin

  before(:each) do
    @user = User.joins(:roles).where("`roles`.`name` = 'admin'").first
  end

  let(:valid_attributes) { @user.attributes }
  let(:valid_session) { {} }

  describe "GET index" do
    it "assigns all users as @users" do
      get :index, {:user => valid_attributes}, valid_session
      assigns(:users).should eq([@user])
    end
    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe "DELETE destroy" do

    before(:each) do
      @non_current_user = User.create(:name => "Example User",
                                      :email => "user@example.com",
                                      :password => "changeme",
                                      :password_confirmation => "changeme")
    end

    it "destroys the requested user" do
      expect {
        delete :destroy, {:id => @non_current_user.to_param}, valid_session
      }.to change(User, :count).by(-1)
    end

    it "redirects to the users list" do
      delete :destroy, {:id => @non_current_user.to_param}, valid_session
      response.should redirect_to(users_url)
    end
  end

  describe "GET 'show'" do

    it "should be successful" do
      get :show, :id => @user.id
      response.should be_success
    end

    it "should find the right user" do
      get :show, :id => @user.id
      assigns(:user).should eq(@user)
    end

  end

end
