# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :mailbox do
    username 'example@example.com'
    password 'password'
    name 'Test Customer'
    maildir 'example'
    quota 1024000
    domain 'example.com'
    created Time.now.utc.to_s
    modified Time.now.utc.to_s
    active false
  end
end
