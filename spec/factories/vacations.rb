# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :vacation do
    email 'example@example.com'
    subject ''
    body ''
    cache ''
    domain ''
    created Time.now.utc.to_s
    active false
  end
end
