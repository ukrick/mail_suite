# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :domain_admin do
    username 'Test Admin'
    domain 'example.com'
    created Time.now.utc.to_s
    active false
  end
end
