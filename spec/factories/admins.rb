# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :admin do
    username 'Test Admin'
    password 'changeme'
    created Time.now.utc.to_s
    modified Time.now.utc.to_s
    active false
  end
end
