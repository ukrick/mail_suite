# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :user do
    name 'Test User'
    email 'example@example.com'
    password 'changeme'
    password_confirmation { |n| n.password }
    # required if the Devise Confirmable module is used
    # confirmed_at Time.now
    after(:create) { |user| user.add_role :user }
  end
end
