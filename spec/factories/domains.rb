# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :domain do
    domain 'example.com'
    description ''
    aliases 1
    mailboxes 1
    maxquota 10
    transport ''
    backupmx false
    created Time.now.utc.to_s
    modified Time.now.utc.to_s
    active false
  end
end
