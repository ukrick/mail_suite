# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :alias do
    address 'example@example.com'
    goto 'alias@example.com'
    domain 'example.com'
    created Time.now.utc.to_s
    modified Time.now.utc.to_s
    active false
  end
end
