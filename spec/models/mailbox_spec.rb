require 'spec_helper'

describe Mailbox do

  describe "asserting number of records" do

    it "has no records in the database" do
      expect(Mailbox).to have(:no).records
      expect(Mailbox).to have(0).records
    end

    it "has one record" do
      Mailbox.create!(username: 'example',
                      password: 'password',
                      name: 'Test Customer',
                      maildir: 'example',
                      quota: 1024000,
                      domain: 'example.com',
                      created: Time.now.utc.to_s,
                      modified: Time.now.utc.to_s)
      expect(Mailbox).to have(1).record
    end

    it "counts only records that match a query" do
      Mailbox.create!(username: 'example',
                      password: 'password',
                      name: 'Test Customer',
                      maildir: 'example',
                      quota: 1024000,
                      domain: 'example.com',
                      created: Time.now.utc.to_s,
                      modified: Time.now.utc.to_s)
      expect(Mailbox.where(:username => 'example@example.com')).to have(1).record
      expect(Mailbox.where(:username => 'example@example.co.uk')).to have(0).records
    end
  end

  describe "validation fields" do

    it "fails validation without required fields (using error_on)" do
      expect(Mailbox.new).to have(1).error_on(:username)
      expect(Mailbox.new).to have(1).error_on(:password)
      expect(Mailbox.new).to have(1).error_on(:domain)
    end

    it "fails validation without required fields expecting a specific message" do
      expect(Mailbox.new.errors_on(:username)).to include("can't be blank")
      expect(Mailbox.new.errors_on(:password)).to include("can't be blank")
      expect(Mailbox.new.errors_on(:domain)).to include("can't be blank")
    end

    it "passes validation with required fields (using 0)" do
      expect(Mailbox.new(:username => 'example@example.com')).to have(0).errors_on(:username)
      expect(Mailbox.new(:password => 'password')).to have(0).errors_on(:password)
      expect(Mailbox.new(:domain => 'example.com')).to have(0).errors_on(:domain)
    end

    it "passes validation with required fields (using :no)" do
      expect(Mailbox.new(:username => 'example@example.com')).to have(:no).errors_on(:username)
      expect(Mailbox.new(:password => 'password')).to have(:no).errors_on(:password)
      expect(Mailbox.new(:domain => 'example.com')).to have(:no).errors_on(:domain)
    end
  end
end
