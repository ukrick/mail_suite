require 'spec_helper'

describe Log do

  describe "asserting number of records" do

    it "has no records in the database" do
      expect(Log).to have(:no).records
      expect(Log).to have(0).records
    end

    it "has one record" do
      create(:log)
      expect(Log).to have(1).record
    end

    it "counts only records that match a query" do
      create(:log)
      expect(Log.where(:timestamp => '2014-03-26 07:15:55')).to have(1).record
      expect(Log.where(:timestamp => Time.now.to_s)).to have(0).records
    end
  end
end
