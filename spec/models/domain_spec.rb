require 'spec_helper'

describe Domain do

  describe "asserting number of records" do

    it "has no records in the database" do
      expect(Domain).to have(:no).records
      expect(Domain).to have(0).records
    end

    it "has one record" do
      create(:domain)
      expect(Domain).to have(1).record
    end

    it "counts only records that match a query" do
      create(:domain)
      expect(Domain.where(:domain => 'example.com')).to have(1).record
      expect(Domain.where(:domain => 'example.co.uk')).to have(0).records
    end
  end

  describe "validation fields" do

    it "fails validation without required fields (using error_on)" do
      expect(Domain.new).to have(1).error_on(:domain)
    end

    it "fails validation without required fields expecting a specific message" do
      expect(Domain.new.errors_on(:domain)).to include("can't be blank")
    end

    it "passes validation with required fields (using 0)" do
      expect(Domain.new(:domain => 'example.com')).to have(0).errors_on(:domain)
    end

    it "passes validation with required fields (using :no)" do
      expect(Domain.new(:domain => 'example.com')).to have(:no).errors_on(:domain)
    end
  end
end
