require 'spec_helper'

describe Vacation do

  describe "asserting number of records" do

    it "has no records in the database" do
      expect(Vacation).to have(:no).records
      expect(Vacation).to have(0).records
    end

    it "has one record" do
      create(:vacation)
      expect(Vacation).to have(1).record
    end

    it "counts only records that match a query" do
      create(:vacation)
      expect(Vacation.where(:email => 'example@example.com')).to have(1).record
      expect(Vacation.where(:email => 'example@example.co.uk')).to have(0).records
    end
  end

  describe "validation fields" do

    it "fails validation without required fields (using error_on)" do
      expect(Vacation.new).to have(1).error_on(:email)
    end

    it "fails validation without required fields expecting a specific message" do
      expect(Vacation.new.errors_on(:email)).to include("can't be blank")
    end

    it "passes validation with required fields (using 0)" do
      expect(Vacation.new(:email => 'example@example.com')).to have(0).errors_on(:email)
    end

    it "passes validation with required fields (using :no)" do
      expect(Vacation.new(:email => 'example@example.com')).to have(:no).errors_on(:email)
    end
  end
end
