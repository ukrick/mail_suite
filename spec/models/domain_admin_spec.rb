require 'spec_helper'

describe DomainAdmin do

  describe "asserting number of records" do

    it "has no records in the database" do
      expect(DomainAdmin).to have(:no).records
      expect(DomainAdmin).to have(0).records
    end

    it "has one record" do
      create(:domain_admin)
      expect(DomainAdmin).to have(1).record
    end

    it "counts only records that match a query" do
      create(:domain_admin)
      expect(DomainAdmin.where(:username => 'Test Admin')).to have(1).record
      expect(DomainAdmin.where(:username => 'Anonymous')).to have(0).records
    end
  end

  describe "validation fields" do

    it "fails validation without required fields (using error_on)" do
      expect(DomainAdmin.new).to have(1).error_on(:username)
      expect(DomainAdmin.new).to have(1).error_on(:domain)
    end

    it "fails validation without required fields expecting a specific message" do
      expect(DomainAdmin.new.errors_on(:username)).to include("can't be blank")
      expect(DomainAdmin.new.errors_on(:domain)).to include("can't be blank")
    end

    it "passes validation with required fields (using 0)" do
      expect(DomainAdmin.new(:username => 'Test Admin')).to have(0).errors_on(:username)
      expect(DomainAdmin.new(:domain => 'example.com')).to have(0).errors_on(:domain)
    end

    it "passes validation with required fields (using :no)" do
      expect(DomainAdmin.new(:username => 'Test Admin')).to have(:no).errors_on(:username)
      expect(DomainAdmin.new(:domain => 'example.com')).to have(:no).errors_on(:domain)
    end
  end
end
