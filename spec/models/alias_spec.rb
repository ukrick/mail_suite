require 'spec_helper'

describe Alias do

  describe "asserting number of records" do

    it "has no aliases in the database" do
      expect(Alias).to have(:no).records
      expect(Alias).to have(0).records
    end

    it "has one record" do
      create(:alias)
      expect(Alias).to have(1).record
    end

    it "counts only records that match a query" do
      create(:alias)
      expect(Alias.where(:address => 'example@example.com')).to have(1).record
      expect(Alias.where(:address => 'test@example.com')).to have(0).records
    end
  end

  describe "validation fields" do

    it "fails validation without required fields (using error_on)" do
      expect(Alias.new).to have(1).error_on(:address)
      expect(Alias.new).to have(1).error_on(:goto)
      expect(Alias.new).to have(1).error_on(:domain)
    end

    it "fails validation without required fields expecting a specific message" do
      expect(Alias.new.errors_on(:address)).to include("can't be blank")
      expect(Alias.new.errors_on(:goto)).to include("can't be blank")
      expect(Alias.new.errors_on(:domain)).to include("can't be blank")
    end

    it "passes validation with required fields (using 0)" do
      expect(Alias.new(:address => 'example@example.com')).to have(0).errors_on(:address)
      expect(Alias.new(:goto => 'alias@example.com')).to have(0).errors_on(:goto)
      expect(Alias.new(:domain => 'example.com')).to have(0).errors_on(:domain)
    end

    it "passes validation with required fields (using :no)" do
      expect(Alias.new(:address => 'example@example.com')).to have(:no).errors_on(:address)
      expect(Alias.new(:goto => 'alias@example.com')).to have(:no).errors_on(:goto)
      expect(Alias.new(:domain => 'example.com')).to have(:no).errors_on(:domain)
    end
  end
end
