require 'spec_helper'

describe Admin do

  describe "asserting number of records" do

    it "has no records in the database" do
      expect(Admin).to have(:no).records
      expect(Admin).to have(0).records
    end

    it "has one record" do
      create(:admin)
      expect(Admin).to have(1).record
    end

    it "counts only records that match a query" do
      create(:admin)
      expect(Admin.where(:username => 'Test Admin')).to have(1).record
      expect(Admin.where(:username => 'Anonymous')).to have(0).records
    end
  end

  describe "validation fields" do

    it "fails validation without required fields (using error_on)" do
      expect(Admin.new).to have(1).error_on(:username)
      expect(Admin.new).to have(1).error_on(:password)
    end

    it "fails validation without required fields expecting a specific message" do
      expect(Admin.new.errors_on(:username)).to include("can't be blank")
      expect(Admin.new.errors_on(:password)).to include("can't be blank")
    end

    it "passes validation with required fields (using 0)" do
      expect(Admin.new(:username => 'Test Admin')).to have(0).errors_on(:username)
      expect(Admin.new(:password => 'changeme')).to have(0).errors_on(:password)
    end

    it "passes validation with required fields (using :no)" do
      expect(Admin.new(:username => 'Test Admin')).to have(:no).errors_on(:username)
      expect(Admin.new(:password => 'changeme')).to have(:no).errors_on(:password)
    end
  end
end
