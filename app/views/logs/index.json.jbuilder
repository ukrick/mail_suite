json.array!(@logs) do |log|
  json.extract! log, :timestamp, :username, :domain, :action, :data
  json.url log_url(log, format: :json)
end
