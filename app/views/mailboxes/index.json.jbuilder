json.array!(@mailboxes) do |mailbox|
  json.extract! mailbox, :username, :name, :maildir, :quota, :domain, :active
  json.url mailbox_url(mailbox, format: :json)
end
