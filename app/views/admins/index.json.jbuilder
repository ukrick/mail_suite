json.array!(@admins) do |admin|
  json.extract! admin, :username, :active
  json.url admin_url(admin, format: :json)
end
