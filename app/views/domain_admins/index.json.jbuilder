json.array!(@domain_admins) do |domain_admin|
  json.extract! domain_admin, :username, :domain, :active
  json.url domain_admin_url(domain_admin, format: :json)
end
