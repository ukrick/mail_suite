json.array!(@vacations) do |vacation|
  json.extract! vacation, :email, :subject, :domain, :active
  json.url vacation_url(vacation, format: :json)
end
