json.array!(@domains) do |domain|
  json.extract! domain, :domain, :description, :aliases, :mailboxes, :maxquota, :transport, :backupmx, :active
  json.url domain_url(domain, format: :json)
end
