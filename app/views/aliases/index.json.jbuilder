json.array!(@aliases) do |a|
  json.extract! a, :address, :goto, :domain, :active
  json.url alias_url(a, format: :json)
end
