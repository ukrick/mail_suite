class Admin < ActiveRecord::Base
  self.table_name = 'admin'

  extend FriendlyId
  friendly_id :username, use: :slugged

  validates :username, presence: true
  validates :password, presence: true

  before_save :crypt_password

  private
  def crypt_password
    self.password = Digest::MD5.hexdigest(self.password)
  end
end
