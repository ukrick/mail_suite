class Log < ActiveRecord::Base
  self.table_name = 'log'

  extend FriendlyId
  friendly_id :timestamp, use: :slugged
end
