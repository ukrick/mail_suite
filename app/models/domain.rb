class Domain < ActiveRecord::Base
  self.table_name = 'domain'

  extend FriendlyId
  friendly_id :domain, use: :slugged

  validates :domain, presence: true
end
