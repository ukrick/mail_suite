class DomainAdmin < ActiveRecord::Base
  self.table_name = 'domain_admins'
  self.primary_key = :username

  extend FriendlyId
  friendly_id :username, use: :slugged

  validates :username, presence: true
  validates :domain, presence: true
end
