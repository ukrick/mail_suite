class Alias < ActiveRecord::Base
  self.table_name = 'alias'

  extend FriendlyId
  friendly_id :address, use: :slugged

  validates :address, presence: true
  validates :goto, presence: true
  validates :domain, presence: true
end
