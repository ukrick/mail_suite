class Vacation < ActiveRecord::Base
  self.table_name = 'vacation'

  extend FriendlyId
  friendly_id :email, use: :slugged

  validates :email, presence: true

  before_save :clean_domain

  private
  def clean_domain
    self.domain = self.domain.gsub('@', '')
  end
end
