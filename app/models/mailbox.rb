class Mailbox < ActiveRecord::Base
  self.table_name = 'mailbox'

  extend FriendlyId
  friendly_id :username, use: :slugged

  validates :username, presence: true
  validates :password, presence: true
  validates :domain, presence: true

  before_save :crypt_password
  before_create :merge_username

  private
  def crypt_password
    self.password = Digest::MD5.hexdigest(self.password)
  end

  def merge_username
    self.username = self.username << '@' << self.domain
    self.slug = self.username.gsub('@', '-').gsub('.', '-')
  end
end
