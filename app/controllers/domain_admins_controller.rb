class DomainAdminsController < ApplicationController
  before_action :set_domain_admin, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
  skip_authorize_resource :only => :show

  # GET /domain_admins
  # GET /domain_admins.json
  def index
    @domain_admins = DomainAdmin.all.page params[:page]
  end

  # GET /domain_admins/1
  # GET /domain_admins/1.json
  def show
  end

  # GET /domain_admins/new
  def new
    @domain_admin = DomainAdmin.new
  end

  # GET /domain_admins/1/edit
  def edit
  end

  # POST /domain_admins
  # POST /domain_admins.json
  def create
    @domain_admin = DomainAdmin.new(domain_admin_params)

    respond_to do |format|
      if @domain_admin.save
        format.html { redirect_to @domain_admin, notice: 'Domain admin was successfully created.' }
        format.json { render action: 'show', status: :created, location: @domain_admin }
      else
        format.html { render action: 'new' }
        format.json { render json: @domain_admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /domain_admins/1
  # PATCH/PUT /domain_admins/1.json
  def update
    respond_to do |format|
      if @domain_admin.update(domain_admin_params)
        format.html { redirect_to @domain_admin, notice: 'Domain admin was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @domain_admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /domain_admins/1
  # DELETE /domain_admins/1.json
  def destroy
    @domain_admin.destroy
    respond_to do |format|
      format.html { redirect_to domain_admins_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_domain_admin
      @domain_admin = DomainAdmin.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def domain_admin_params
      params.require(:domain_admin).permit(:username, :domain, :created, :active)
    end
end
