class LogsController < ApplicationController
  before_action :set_log, only: [:show, :destroy]

  load_and_authorize_resource
  skip_authorize_resource :only => :show

  # GET /logs
  # GET /logs.json
  def index
    @logs = Log.all.page params[:page]
  end

  # GET /logs/1
  # GET /logs/1.json
  def show
  end

  # DELETE /logs/1
  # DELETE /logs/1.json
  def destroy
    @log.destroy
    respond_to do |format|
      format.html { redirect_to logs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_log
      @log = Log.friendly.find(params[:id])
    end
end
