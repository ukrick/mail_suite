# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready page:load', ->
  action = $('#action').data 'action'
  email = $('#vacation_email')
  domain = $('#vacation_domain')
  domain.prop readonly: true
  if action is 'edit'
    email.prop disabled: true
    $('.vacation_domain').hide()
  email.change ->
    val = $(':selected', email).text()
    domain.prop value: val.substring(val.indexOf('@'))
