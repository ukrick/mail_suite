# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready page:load', ->
  action = $('#action').data 'action'
  email = $('#mailbox_username')
  domain = $('.mailbox_domain')
  $('option', domain).each ->
    if this.index isnt 0
      this.text = '@' + this.text
  if action is 'edit'
    email.prop readonly: true
    domain.hide()
  if action is 'update'
    email.prop readonly: true
    domain.hide()