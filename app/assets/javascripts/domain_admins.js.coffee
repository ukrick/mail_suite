# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready page:load', ->
  action = $('#action').data 'action'
  domain_admin = $('#domain_admin_username')
  if action is 'edit' or action is 'update'
    domain_admin.prop disabled: true