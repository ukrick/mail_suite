MailSuite::Application.routes.draw do
  resources :vacations

  resources :mailboxes

  resources :logs

  resources :domain_admins

  resources :domains

  resources :aliases

  resources :admins

  root :to => "home#index"
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users
end